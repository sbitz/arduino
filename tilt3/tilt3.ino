int xPin= 6;
int yPin= 9;
int zPin= 11;

unsigned int xVal, yVal, zVal;

void setup() {
   Serial.begin(9600);
   pinMode(xPin, INPUT);
   pinMode(yPin, INPUT);
   pinMode(zPin, INPUT);
}

void loop() {
  xVal= analogRead(xPin);
  yVal= analogRead(yPin);
  zVal= analogRead(zPin);
  Serial.print("(x,y,z)= ("+xVal+","+yVal+","+zVal+")");
  delay(500);
}
