
/*
  Esplora Blink
 
 This  sketch blinks the Esplora's RGB LED. It goes through
 all three primary colors (red, green, blue), then it 
 combines them for secondary colors(yellow, cyan, magenta), then
 it turns on all the colors for white. 
 For best results cover the LED with a piece of white paper to see the colors.
 
 Created on 22 Dec 2012
 by Tom Igoe
 
 This example is in the public domain.
 */

#include <Esplora.h>

byte count= 0;
byte bright;

void setup() {
}

byte getBrightness() {
  return Esplora.readSlider()/4;
}



void loop() {
  
  count++;
  if (count == 8)
    count -=7;
  bright= getBrightness();
  Serial.print("Brightness: ");
  Serial.print(bright);
  Serial.print(", Count: ");
  Serial.print(count); 
  Serial.print("\n");
 byte r= (count & 4) * bright,
       g= (count & 2) * bright,
       b= (count & 1) * bright;
  Esplora.writeRGB(r,g,b);    // make the LED red
  delay(1000);                  // wait 1 second
}

//
