int pingPin= 9;
int echoPin= 11;

unsigned int duration, inches;

void setup() {
Serial.begin(9600);
pinMode(pingPin, OUTPUT);
pinMode(echoPin, INPUT);
}

void loop() {
digitalWrite(pingPin, LOW);
delayMicroseconds(2);
digitalWrite(pingPin, HIGH);
delayMicroseconds(10);
digitalWrite(pingPin, LOW);
duration= pulseIn(echoPin, HIGH);
inches = duration / 74 / 2;
Serial.println(inches);
delay(500);
}
